/* global $ */
(function() {
    
  $ = function(selector) {
      if(!(this instanceof $)) {
          return new $(selector);
      }
      
      if(typeof selector === "string") {
        var items = document.querySelectorAll(selector);
      } else {
          items = selector;
      }
      [].push.apply(this, items);      
  };

  $.extend = function(target, object) {   
      for(var p in object) {
          if(object.hasOwnProperty(p))
            target[p] = object[p];
      }
      return target;
  };

  // Static methods
  var isArrayLike = function(obj) {
      if(typeof obj.length === "number") {
          if(obj.length === 0) {
              return true;
          } else if(obj.length > 0) {
              return (obj.length - 1) in obj;
          }
      }    
      return false;
  };

  $.extend($, {
    isArray: function(obj) {
        return Object.prototype.toString.call(obj) === "[object Array]";
    },
    each: function(collection, cb) {
        if(isArrayLike(collection)) {
            for(var i = 0; i < collection.length; i++) {
                cb.call(collection[i], i, collection[i]);
            }
        } else {
            for(var p in collection) {
                if(collection.hasOwnProperty(p)) {
                    var value = collection[p];
                    cb.call(value, p, value);
                }
            }
        }
        return collection;
    },
    makeArray: function(arr) {
        var newArr = [];
        $.each(arr, function(i, v) {
            newArr.push(v);
        });
        return newArr;
    },
    proxy: function(fn, context) {
        return function() {
            return fn.apply(context, arguments);
        }
    },
    fn: $.prototype   
  });
  
  var getText = function(el) {
      var txt = "";
      $.each(el.childNodes, function(i, childNode) {
          if(childNode.nodeType === Node.TEXT_NODE) {
              txt += childNode.nodeValue;
          } else if(childNode.nodeType === Node.ELEMENT_NODE) {
              txt += getText(childNode);
          }
      });      
      return txt;
  }
  
  var makeTraverser = function(cb) {
      return function() {
          var elements = [], args = arguments;       
          $.each(this, function(i, el) {
              var res = cb.apply(el, args);
              if(res && isArrayLike(res)) {
                  [].push.apply(elements, res);
              } else if(res) {
                  elements.push(res);
              }
          });       
          return $(elements)
      }
  }

  $.extend($.prototype, {
    html: function(newHtml) {
        if(arguments.length) {
            //setter
            $.each(this, function(i, el) {
               el.innerHTML = newHtml;
            });
            return this;
        } else {
            //getter
            return this[0].innerHTML;
        }
        
    },
    val: function(newVal) {
        if(arguments.length) {
            //setter
            $.each(this, function(i, el) {
                el.value = newVal;
            });
            return this;
        } else {
            //getter
            return this[0] && this[0].value;
        }
    },
    text: function(newText) {      
        if(arguments.length) {
            //setter
            this.html("");
            $.each(this, function(i, el) {
               var txtNode = document.createTextNode(newText);
               el.appendChild(txtNode);
            });
            return this;
        } else {
            //getter
            return this[0] && getText(this[0]);
        }
    },
    find: function(selector) {
        var elements = [];
         $.each(this, function(i, ele) {          
            var elems = ele.querySelectorAll(selector);
            $.each(elems, function(i, el) {
                elements.push(el);
            });
            //[].push.apply(elements, elems);
        });      
        return $(elements)
    },
    next: makeTraverser(function() {
        var nxt = this.nextSibling;
        while(nxt && nxt.nodeType !== Node.ELEMENT_NODE) {
            nxt = nxt.nextSibling;             
        }     
        if(nxt) {
            return nxt;
        }
    }),
    prev: makeTraverser(function() {
        var prv = this.previousSibling;
        while(prv && prv.nodeType !== Node.ELEMENT_NODE) {
            prv = prv.previousSibling;             
        }     
        if(prv) {
            return prv;
        }
    }),
    parent: makeTraverser(function() {
        return this.parentNode;
    }),
    children: makeTraverser(function() {
        return this.children;  
    }),
    attr: function(attrName, value) {
        if(arguments.length === 2) {
            //set
            return $.each(this, function(i, el) {
               el.setAttribute(attrName, value);
            });
        } else if(arguments.length === 1) {
            //get
            return this[0] && this[0].getAttribute(attrName);
        }
    },
    css: function(cssPropName, value) {
        if(arguments.length === 2) {
            //set
            return $.each(this, function(i, el) {
               el.style[cssPropName] = value;
            });
        } else if(arguments.length === 1) {
            //get
            return this[0] && document
                    .defaultView
                    .getComputedStyle(this[0])
                    .getPropertyValue(cssPropName);
        }
    },
    width: function() {
        var clientWidth = this[0].clientWidth;
        var leftPadding = this.css("padding-left"),
            rightPadding = this.css("padding-right");          
        return clientWidth - (parseInt(leftPadding, 10) + parseInt(rightPadding, 10));                
    },
    offset: function() {
      var offset = this[0].getBoundingClientRect();
      return {
        top: offset.top + window.pageYOffset,
        left: offset.left + window.pageXOffset
      };
    },
    hide: function() {
        return this.css("display", "none");
    },
    show: function() {
        return this.css("display", "");
    },

    // Events
    bind: function(eventName, handler) {
        return $.each(this, function(i, el) {
            el.addEventListener(eventName, handler, false);
        });
    },
    unbind: function(eventName, handler) {
        return $.each(this, function(i, el) {
            el.removeEventListener(eventName, handler, false);
        });
    },
    has: function(selector) {
      var elements = [];
	
      $.each(this, function(i, el) {
        if(el.matches(selector)) {
          elements.push(el);
        }
      });
    
      return $( elements );
    },
    on: function(eventType, selector, handler) {
      return this.bind(eventType, function(ev){
        var cur = ev.target;
        do {
          if ($([ cur ]).has(selector).length) {
            handler.call(cur, ev);
          }
          cur = cur.parentNode;
        } while (cur && cur !== ev.currentTarget);
      });
    },
    off: function(eventType, selector, handler) {},
    data: function(propName, data) {
         if(arguments.length === 2) {
            return $.each(this, function(i, el) {
                el.dataset.propName = data;
            });    
         } else {
            return this[0] && this[0].dataset.propName;
         }
    },

    // Extra
    addClass: function(className) {
        return this.attr("class", className);
    },
    removeClass: function(className) {
        return this.attr("class", "");
    },
    append: function(element) {
        return this[0].innerHTML += element;
    }
  });

  $.buildFragment = function(html) {};
  
})();